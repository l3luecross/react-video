// import { useState } from 'react';
import Grid from '@mui/material/Unstable_Grid2';
import {
  Card,
  CardActionArea,
  CardMedia,
  IconButton,
} from '@mui/material';
import CardCover from '@mui/joy/CardCover';

import { Link } from "react-router-dom"
import PlayCircleFilledWhiteIcon from '@mui/icons-material/PlayCircleFilledWhite';

import video from "../../mocks/videoLists.json"

const Main = () => {
  // const [mouseHover, setMouseHover] = useState(false);
  return (
    <Grid container  style={{marginTop:20, marginLeft:20, marginRight:20}} spacing={{ xs: 4, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
        {video.map((item, index) => (
          <Grid xs={4} sm={4} md={4} key={index}>
            <Link
              to={`/video?${item.path}`}
              key={index}
            >
              <Card
                key={index}
                // onMouseEnter={() => setMouseHover(true)}
                // onMouseLeave={() => setMouseHover(false)}
              >
                <CardActionArea>
                  {/* {mouseHover=== true? */}
                    <CardCover
                      sx={{
                        background:
                          'linear-gradient(to top, rgba(0,0,0,0.4), rgba(0,0,0,0) 200px), linear-gradient(to top, rgba(0,0,0,0.8), rgba(0,0,0,0) 300px)',
                        border: '1px solid',
                        borderColor: '#777',
                      }}
                    >
                      <IconButton>
                          <PlayCircleFilledWhiteIcon
                              sx={{
                                 width:50,
                                 height:50,
                                color:'white'}}
                          />
                      </IconButton>
                    </CardCover>
                    {/* :null
                  } */}
                  <CardMedia
                      component="img"
                      height="auto"
                      image={item.thumbnail}
                    />
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        ))}
  </Grid>
   );
}
export default Main;