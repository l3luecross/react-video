import ReactPlayer from 'react-player/youtube';
import { useSearchParams } from "react-router-dom";
import { useParams } from 'react-router';
import Grid from '@mui/material/Unstable_Grid2';

const Contain = () => {
    const params= useParams();
    const [searchParams] = useSearchParams(params);
    const id = searchParams.get('id');
    const videoId = `https://www.youtube.com/watch?v=${id}`
    const videoConfig = {
      youtube: {
        playerVars: {
           autoplay: 1
        }
      },
    };
    return (
      <Grid container spacing={0} justifyContent="center" alignItems="center">
        <Grid item xs={12} sm={10} md={8} lg={6} >
        <ReactPlayer
                    width={'auto'}
                    controls={true}
                    url={videoId}
                    config={videoConfig}
                  />
        </Grid>
      </Grid>
      );
}
export default Contain;
