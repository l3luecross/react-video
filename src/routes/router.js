import { useRoutes } from "react-router-dom";
import Contain from "../views/contain/contain";
import Main from "../views/main/main";
import FullLayout from "../layouts/fullLayout"

const Routes = () => {
    const routes = useRoutes([
      { path: "/",
        element: <FullLayout/>,
         children: [
          { path: "", element: <Main/> },
          { path: ":id", element: <Contain/>  }
         ],
      }
    ]);
    return routes;
  }
export default Routes;