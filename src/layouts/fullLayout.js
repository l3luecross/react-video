import React from "react";
import {  Outlet, } from 'react-router-dom';
import Header from "./header/header";
import{
    Box,
    Container
} from "@mui/material";

const FullLayout = () => {
    return (
      <>
        <Header />
        <Container maxWidth="xl" style={{ marginTop:20}}>
         <Box sx={{ height: '550px' }} >
                <Outlet />
         </Box>
        </Container>
    </>
    );
};
export default FullLayout;
